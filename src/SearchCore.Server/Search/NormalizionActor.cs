﻿using System;
using Akka.Actor;
using SearchCore.Server.Models;

namespace SearchCore.Server.Search
{
    public class NormalizionActor : ReceiveActor
    {
        public NormalizionActor()
        {
            Receive<string>(msg => Debug(msg));

            Receive<SearchToursInputModel>(msg => Normalize(msg));
        }

        private void Debug(string msg)
        {
            Console.WriteLine($"{Self} - {msg}");
        }

        private void Normalize(SearchToursInputModel msg)
        {

        }

        public static Props Props => Props.Create<NormalizionActor>();
    }
}
