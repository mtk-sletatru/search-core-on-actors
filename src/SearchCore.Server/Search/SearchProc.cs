﻿using System;
using Akka.Actor;

namespace SearchCore.Server.Search
{
    public static class SearchProc
    {
        private static ActorSystem _actorSystem;

        public static IActorRef Queue;

        public static void Start()
        {
            _actorSystem = ActorSystem.Create(nameof(SearchProc));
            Queue = _actorSystem.ActorOf(NormalizionActor.Props, nameof(NormalizionActor));
            Console.WriteLine("search system started");
        }

        public static void Stop()
        {
            _actorSystem?.Dispose();
        }
    }
}
