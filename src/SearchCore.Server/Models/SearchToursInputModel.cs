﻿using System;

namespace SearchCore.Server.Models
{
    public class SearchToursInputModel
    {
        public int RequestId { get; set; }
        public string UserId { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int CountryId { get; set; }
        public int CityFromId { get; set; }
        public string Cities { get; set; }
        public string Meals { get; set; }
        public string Stars { get; set; }
        public string Hotels { get; set; }
        public int AdultsCount { get; set; }
        public int KidsCount { get; set; }
        public int[] KidsAges { get; set; }
        public int NightsCountMin { get; set; }
        public int NightsMax { get; set; }
        public int PriceMin { get; set; }
        public int PriceMax { get; set; }
        public string CurrencyAlias { get; set; }
        public DateTime DepartFrom { get; set; }
        public DateTime DepartTo { get; set; }
        public int[] VisibleOperators { get; set; }
        public int[] HiddenOperators { get; set; }
        public bool s_hotelIsNotInStop { get; set; }
        public bool HasTickets { get; set; }
        public bool TicketsIncludedInPrice { get; set; }
        public int RequestTimeoutMs { get; set; }
        public string s_showcase { get; set; }
        public string fake { get; set; }
        public string updateResult { get; set; }
        public int vk_group_id { get; set; }
        public int debug { get; set; }
        public int filter { get; set; }
        public string f_to_id { get; set; }
        public string templateName { get; set; }
        public string useTree { get; set; }
        public string groupBy { get; set; }
        public int includeDescriptions { get; set; }
        public string sessionId { get; set; }
        public int economOnly { get; set; }
        public int includeOilTaxesAndVisa { get; set; }
        public int jskey { get; set; }
        public string cacheMode { get; set; }
        public int requestIdToTakeFrom { get; set; }
        public string referer { get; set; }
        public int hideShopTours { get; set; }
        public string login { get; set; }
        public string password { get; set; }
        public string lang { get; set; }
        public string target { get; set; }
        public string siteSessionId { get; set; }
        public string useAccount { get; set; }
        public string minHotelRating { get; set; }
        public string beachLines { get; set; }
        public int calcFullPrice { get; set; }
        public int showHotelFacilities { get; set; }
        public int isHotTourWidget { get; set; }
        public string rtb { get; set; }
    }
}
