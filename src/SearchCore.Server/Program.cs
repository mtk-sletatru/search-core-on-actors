﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SearchCore.Server.Api;
using SearchCore.Server.Search;

namespace SearchCore.Server
{
    class Program
    {
        static void Main(string[] args)
        {
            SearchProc.Start();
            ApiProc.Start();

            Console.ReadLine();

            ApiProc.Stop();
            SearchProc.Stop();
        }
    }
}
