﻿using System;
using Microsoft.Owin.Hosting;

namespace SearchCore.Server.Api
{
    public static class ApiProc
    {
        private static IDisposable _apiObj;

        public static void Start()
        {
            _apiObj = WebApp.Start<Startup>("http://localhost:7665/");
            Console.WriteLine("api started");
        }

        public static void Stop()
        {
            _apiObj?.Dispose();
        }
    }
}
