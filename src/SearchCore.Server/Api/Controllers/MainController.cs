﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Akka.Actor;
using SearchCore.Server.Models;
using SearchCore.Server.Search;

namespace SearchCore.Server.Api.Controllers
{
    [RoutePrefix("api")]
    public class MainController : ApiController
    {
        [Route("tour/healthcheck")]
        [HttpGet]
        public string HealthCheck()
        {
            return "hi, i'm fine";
        }

        [Route("tour/search")]
        [HttpPost]
        public HttpResponseMessage SearchTours(SearchToursInputModel input)
        {
            SearchProc.Queue.Tell(input);
            return new HttpResponseMessage(HttpStatusCode.Accepted);
        }

        [Route("tour/searchstate")]
        [HttpPost]
        public string GetSearchToursState()
        {
            throw new NotImplementedException();
        }
    }
}
