﻿using System;
using System.Runtime.Serialization;
using ProtoBuf;
using SletatRu.Common;

namespace SearchCore.Server.PluginHost
{
    [DataContract(Namespace = "urn:SletatRu:DataTypes:ChewerClient:GetToursRequest:v1")]
    [ProtoContract]
    public sealed class GetToursRequest
    {
        [DataMember]
        [ProtoMember(1)]
        public int RequestCode;
        [DataMember]
        [ProtoMember(2)]
        public string CoreAddress;
        [DataMember]
        [ProtoMember(3)]
        public string PluginName;
        [DataMember]
        [ProtoMember(4)]
        public int CountryId;
        [DataMember]
        [ProtoMember(5)]
        public int TownFromId;
        [DataMember]
        [ProtoMember(6)]
        public int SourceId;
        [DataMember]
        [ProtoMember(7)]
        public int[] Towns;
        [DataMember]
        [ProtoMember(8)]
        public int[] Meals;
        [DataMember]
        [ProtoMember(9)]
        public int[] Stars;
        [DataMember]
        [ProtoMember(10)]
        public int[] Hotels;
        [DataMember]
        [ProtoMember(11)]
        public byte Adults;
        [DataMember]
        [ProtoMember(12)]
        public byte Kids;
        [DataMember]
        [ProtoMember(13)]
        public byte[] KidsAges;
        [DataMember]
        [ProtoMember(14)]
        public byte NightsMin;
        [DataMember]
        [ProtoMember(15)]
        public byte NightsMax;
        [DataMember]
        [ProtoMember(16)]
        public DateTime CheckInFrom;
        [DataMember]
        [ProtoMember(17)]
        public DateTime CheckInTo;
        [DataMember]
        [ProtoMember(18)]
        public int? PriceMin;
        [DataMember]
        [ProtoMember(19)]
        public int? PriceMax;
        [DataMember]
        [ProtoMember(20)]
        public byte CurrencyId;
        [DataMember]
        [ProtoMember(21)]
        public bool HotelIsNotInStop;
        [DataMember]
        [ProtoMember(22)]
        public bool TicketsIncluded;
        [DataMember]
        [ProtoMember(23)]
        public bool HasTickets;
        [DataMember]
        [ProtoMember(24)]
        public bool IsOriginalHotels;
        [DataMember]
        [ProtoMember(25)]
        public bool UseTownTree;
        [DataMember]
        [ProtoMember(26)]
        public bool HideShopTours;
        [DataMember]
        [ProtoMember(27)]
        public short RowsToLoad;
        [DataMember]
        [ProtoMember(28)]
        public short Timeout;
        [DataMember]
        [ProtoMember(29)]
        public string Login;
        [DataMember]
        [ProtoMember(30)]
        public string Password;
        [DataMember]
        [ProtoMember(31)]
        public RequestOrigin RequestOrigin;
    }
}